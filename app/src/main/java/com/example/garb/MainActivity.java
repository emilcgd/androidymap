package com.example.garb;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.yandex.mapkit.Animation;
import com.yandex.mapkit.MapKitFactory;
import com.yandex.mapkit.geometry.Point;
import com.yandex.mapkit.map.CameraPosition;
import com.yandex.mapkit.mapview.MapView;

import org.w3c.dom.Text;

import java.util.Random;
import java.util.function.DoubleToIntFunction;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button button1, button2, button3;
    TextView textView1, textView2;
    private MapView mapView;
    SQLiteDatabase db;
    DBHelper dbHelper;
    Intent switchActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        MapKitFactory.setApiKey("8e59f44f-854f-4bcb-b8fc-2f5976e32031");
        MapKitFactory.initialize(this);
        setContentView(R.layout.activity_main);

        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);


        textView1 = (TextView) findViewById(R.id.textView1);

        mapView = (MapView) findViewById(R.id.mapview);
        mapView.getMap().move(
                new CameraPosition(new Point(54.7431, 55.9678), 11.0f, 0.0f, 0.0f),
                new Animation(Animation.Type.SMOOTH, 0), null);

        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);

        dbHelper = new DBHelper(this);


    }

    @Override
    public void onClick(View v) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        ContentValues content = new ContentValues();
        String latitude;
        String longitude;

        switch(v.getId()){
            //Показыать координаты точки
            case R.id.button1:
                latitude = String.valueOf(mapView.getMap().getCameraPosition().getTarget().getLatitude());
                longitude = String.valueOf(mapView.getMap().getCameraPosition().getTarget().getLongitude());
                textView1.setText(latitude + " " + longitude);
                break;

            //Добавление метки и добавление координаты в БД
            case R.id.button2:
                latitude = String.valueOf(mapView.getMap().getCameraPosition().getTarget().getLatitude());
                longitude = String.valueOf(mapView.getMap().getCameraPosition().getTarget().getLongitude());
                content.put(dbHelper.KEY_LATITUDE, latitude);
                content.put(dbHelper.KEY_LONGITUDE, longitude);
                database.insert(dbHelper.TABLE_PLACEMARKS, null, content);

                mapView.getMap().getMapObjects().addPlacemark(new Point(mapView.getMap().getCameraPosition().getTarget().getLatitude(), mapView.getMap().getCameraPosition().getTarget().getLongitude()));

                break;

            //Вывод БД в Логи
            case R.id.button3:
                setContentView(R.layout.activity_db);
                Cursor cursor = database.query(dbHelper.TABLE_PLACEMARKS, null,null,null,null,null,null,null);
                if (cursor.moveToFirst()) {
                    int idIndex = cursor.getColumnIndex(dbHelper.KEY_ID);
                    int latIndex = cursor.getColumnIndex(dbHelper.KEY_LATITUDE);
                    int longIndex = cursor.getColumnIndex(dbHelper.KEY_LONGITUDE);
                    do {
                        Log.d("mLog", "ID = " + cursor.getInt(idIndex) +
                                ", lat = " + cursor.getString(latIndex) +
                                ", long = " + cursor.getString(longIndex));
                    } while (cursor.moveToNext());
                } else
                    Log.d("mLog", "0 rows");
                cursor.close();

                switchActivity = new Intent(getApplicationContext(), DbActivity.class);
                startActivity(switchActivity);
                break;

        }
        dbHelper.close();
    }



    @Override
    protected void onStart() {
        super.onStart();

        mapView.onStart();
        MapKitFactory.getInstance().onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
        MapKitFactory.getInstance().onStop();
    }

}
