package com.example.garb;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class DbActivity extends AppCompatActivity implements View.OnClickListener{

    Button button5;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        button5 = (Button) findViewById(R.id.button5);
        button5.setOnClickListener(this);
    }

    protected void onStart() {
        super.onStart();

    }


    protected void onStop() {
        super.onStop();
    }

    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button5:
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                break;
        }
    }
}
